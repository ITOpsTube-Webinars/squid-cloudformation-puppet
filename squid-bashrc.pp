# squidnodebashrc class to configure proxy settings
class { 'squidnodebashrc': }

class squidnodebashrc {
  # exporting proxy_http to ec2-user bashrc
  file_line { 'export ec2-user proxy_http':
    path => '/home/ec2-user/.bashrc',  
    line => 'export http_proxy=http://server-ip:8080/',
    match   => '^export http_proxy=.*$',
  }
  # exporting proxy_https to ec2-user bashrc
  file_line { 'export ec2-user proxy_https':
    path => '/home/ec2-user/.bashrc',  
    line => 'export https_proxy=http://server-ip:8080/',
    match   => '^export https_proxy=.*$',
  }
  # exporting proxy_http to root bashrc
  file_line { 'export root proxy_http':
    path => '/root/.bashrc',  
    line => 'export http_proxy=http://server-ip:8080/',
    match   => '^export http_proxy=.*$',
  }
  # exporting proxy_https to root bashrc
  file_line { 'export root proxy_https':
    path => '/root/.bashrc',  
    line => 'export https_proxy=http://server-ip:8080/',
    match   => '^export https_proxy=.*$',
  }
  # Injecting proxy to yum configuration 
  file_line { 'enable Proxy Settings for Yum':
    path => '/etc/yum.conf',  
    line => 'proxy=http://server-ip:8080/',
    match   => '^proxy=.*$',
    after   => '^[main]',
  }
}