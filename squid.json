{
    "Description": "Create nginx ec2 instance.",
    "AWSTemplateFormatVersion": "2010-09-09",
    "Parameters": {
      "TagKey": {
        "Description": "The EC2 tag key that identifies this as a target for deployments.",
        "Type": "String",
        "Default": "Name",
        "AllowedPattern": "[\\x20-\\x7E]*",
        "ConstraintDescription": "Can contain only ASCII characters."
      },
      "TagValue": {
        "Description": "The EC2 tag value that identifies this as a target for deployments.",
        "Type": "String",
        "Default": "Squid",
        "AllowedPattern": "[\\x20-\\x7E]*",
        "ConstraintDescription": "Can contain only ASCII characters."
      },
      "KeyPairName": {
        "Description": "Name of an existing Amazon EC2 key pair to enable SSH",
        "Type": "String",
        "Default": "demo",
        "MinLength": "1",
        "MaxLength": "255",
        "AllowedPattern": "[\\x20-\\x7E]*",
        "ConstraintDescription": "KeyPairName is a required Field and can contain only ASCII characters."
      },
      "InstanceType": {
        "Description": "Amazon EC2 instance type.",
        "Type": "String",
        "Default": "t2.medium",
        "ConstraintDescription": "Must be a valid Amazon EC2 instance type."
      },
      "InstanceCount": {
        "Description": "Number of Amazon EC2 instances (Must be a number between 1 and 3).",
        "Type": "Number",
        "Default": "1",
        "ConstraintDescription": "Must be a number between 1 and 3.",
        "MinValue": "1",
        "MaxValue": "3"
      },
      "OperatingSystem": {
        "Description": "Amazon EC2 operating system type (Linux or Redhat).",
        "Type": "String",
        "Default": "Redhat",
        "ConstraintDescription": "Must be Redhat or Linux. (Linux means Amazon linux)",
        "AllowedValues": [
          "Linux",
          "Redhat"
        ]
      },
      "SSHLocation": {
        "Description": "The IP address range that can be used to connect using SSH to the Amazon EC2 instances.",
        "Type": "String",
        "MinLength": "9",
        "MaxLength": "18",
        "Default": "0.0.0.0/0",
        "AllowedPattern": "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})",
        "ConstraintDescription": "Must be a valid IP CIDR range of the form x.x.x.x/x."
      }
    },

    "Mappings": {
        "RegionOS2AMI": {
          "us-east-1": {
            "Linux": "ami-0b898040803850657",
            "Redhat": "ami-2051294a"
          },
          "us-west-2": {
            "Linux": "ami-55a7ea65",
            "Redhat": "ami-775e4f16"
          }
        },
        "OS2SSHPort": {
          "Linux": {
            "SSHPort": "22"
          },
          "Redhat": {
            "SSHPort": "22"
          }
        }
    },

    "Resources": {
        "LinuxEC2Instance": {
          "Type": "AWS::EC2::Instance",
          "Properties": {
            "ImageId": {
              "Fn::FindInMap": [
                "RegionOS2AMI",
                {
                  "Ref": "AWS::Region"
                },
                {
                  "Ref": "OperatingSystem"
                }
              ]
            },
            "InstanceType": {
              "Ref": "InstanceType"
            },
            "SecurityGroups": [
              {
                "Ref": "SecurityGroup"
              }
            ],
            "UserData": {
              "Fn::Base64": {
                "Fn::Join": [
                  "",
                  [
                    "#!/bin/bash -ex\n",
                    "setenforce 0\n",
                    "yum install https://yum.puppet.com/puppet6-release-el-7.noarch.rpm -y\n",
                    "yum install -y git puppetserver puppet-agent\n",
                    "systemctl start puppetserver\n",
                    "systemctl enable puppetserver\n",
                    "echo '[main]' >> /etc/puppetlabs/puppet/puppet.conf\n",
                    "echo \"certname = $(hostname -f)\" >> /etc/puppetlabs/puppet/puppet.conf\n",
                    "echo \"server = $(hostname -f)\" >> /etc/puppetlabs/puppet/puppet.conf\n",
                    "echo 'environment = production' >> /etc/puppetlabs/puppet/puppet.conf\n",
                    "echo 'runinterval = 15m' >> /etc/puppetlabs/puppet/puppet.conf\n",
                    "cd /home/ec2-user\n",
                    "git clone https://gitlab.com/ITOpsTube-Webinars/squid-cloudformation-puppet.git\n",
                    "cd /home/ec2-user/squid-cloudformation-puppet\n",
                    "touch executed\n",
                    "/opt/puppetlabs/bin/puppet resource service puppet ensure=running enable=true\n",
                    "/opt/puppetlabs/bin/puppet module install puppet-squid\n",
                    "/opt/puppetlabs/bin/puppet module install puppet-archive\n",
                    "mkdir -p /etc/puppetlabs/code/environments/production/manifests\n",
                    "cp squid.pp /etc/puppetlabs/code/environments/production/manifests\n",
                    "/opt/puppetlabs/bin/puppet agent --test || true\n",
                    "touch executed-success\n",
                    "yum remove -y puppetserver puppet-agent\n",
                    "init 6\n",
                  ]
                ]
              }
            },
            "KeyName": {
              "Ref": "KeyPairName"
            },
            "Tags": [
              {
                "Key": {
                  "Ref": "TagKey"
                },
                "Value": {
                  "Ref": "TagValue"
                }
              }
            ],
          }
        },

        "SecurityGroup": {
            "Type": "AWS::EC2::SecurityGroup",
            "Properties": {
              "GroupDescription": "Enable HTTP access via port 80 and SSH access.",
              "SecurityGroupIngress": [
                {
                  "IpProtocol": "tcp",
                  "FromPort": "80",
                  "ToPort": "80",
                  "CidrIp": "0.0.0.0/0"
                },
                {
                    "IpProtocol": "tcp",
                    "FromPort": "22",
                    "ToPort": "22",
                    "CidrIp": "0.0.0.0/0"
                  }
              ]
            }
        }

    }
}
