class { 'linux': }

class linux {
  $packagetools=['unzip', 'wget', 'git']
  # Install Some basic packages
  package{ $packagetools:
    ensure => 'present',
  }
  # Upgrade openssl package to the latest
  $packageopenssl = ['openssl']
  package{ $packageopenssl:
    ensure => 'latest',
  }
  # Making selinux permissive
  file_line { 'Append disable selinux to /etc/selinux/config':
    path => '/etc/selinux/config',  
    line => 'SELINUX=permissive',
    match   => '^SELINUX=.*$',
  }
  # Disabling ipv6 under grub
  file_line { 'Append disable ipv6 to /etc/default/grub':
    path => '/etc/default/grub',  
    line => 'GRUB_CMDLINE_LINUX="ipv6.disable=1 console=ttyS0,115200n8 console=tty0 net.ifnames=0 crashkernel=auto"',
    match   => '^GRUB_CMDLINE_LINUX="console=ttyS0,115200n8 console=tty0 net.ifnames=0 crashkernel=auto".*$',
  }
  # Rebuilding grub configuration
  exec { 'grub2-mkconfig -o /boot/grub2/grub.cfg':
    command => 'grub2-mkconfig -o /boot/grub2/grub.cfg',
    provider => shell,
    creates => '/etc/squid/certs/squid-ca-cert-key.pem',
  }
  # Generating Squid certificate for https proxy
  exec { 'Generate a Self Signed OpenSSL certificate':
    command => 'openssl req -new -newkey rsa:2048 -sha256 -days 365 -nodes -x509 -extensions v3_ca -keyout /home/ec2-user/squid-ca-key.pem -out /home/ec2-user/squid-ca-cert.pem -subj "/C=US/ST=Maryland/L=Columbia/O=CMS/OU=Admin/CN=squid.cms.com"',
    provider => shell,
    creates => '/etc/squid/certs/squid-ca-cert-key.pem',
  }
}

# prebuild squid module
# make sure that squid module was installed on puppet master, to install run below command on pupper master
# /opt/puppetlabs/bin/puppet module install puppet-squid

# Squid class to install squid and configure
class { 'squid': 
  coredump_dir => /var/spool/squid
}

# Updating ACL SSL_ports on Squid.conf 
squid::acl { 'SSL_ports':
   type    => 'port',
   entries => ['443'],
}

# Updating ACL allowed_sites on Squid.conf 
squid::acl { 'allowed_sites':
   type    => 'dstdomain',
   entries => ['.google.com'],
}

# Updating ACL allowed_sites on Squid.conf
squid::acl { 'allowed_sites':
   type    => 'dstdomain',
   entries => ['.yahoo.com'],
}
# Updating ACL all_others on Squid.conf
squid::acl { 'all_others':
   type    => 'dst',
   entries => ['all'],
}

# Updating allowed_sites to allow on Squid.conf
squid::http_access { 'allowed_sites':
  action => allow,
}
# Updating all_others to deny on Squid.conf
squid::http_access{ 'all_others':
  action => deny,
}
# Updating localhost manager to allow on Squid.conf
squid::http_access { 'localhost manager':
  action => allow,
}
# Updating manager to deny from outside on Squid.conf
squid::http_access { 'manager':
  action => deny,
}
# Updating localhost to allow only from localhost on Squid.conf
squid::http_access { 'localhost':
  action => allow,
}
# configuring squid port 8080 on Squid.conf
squid::http_port { '8080':
  options => 'ssl-bump cert=/etc/squid/certs/squid-ca-cert-key.pem generate-host-certificates=on dynamic_cert_mem_cache_size=16MB'
}
# Updating ACL SslBump1 on Squid.conf
squid::acl { 'step1':
   type    => 'at_step',
   entries => ['SslBump1'],
}
# Updating extra_config_section peek and step1 on Squid.conf
squid::extra_config_section { 'ssl_bump settings peek':
  config_entries => {
    'ssl_bump'         => ['peek', 'step1'],
  }
}
# Updating ssl_bump settings for  splice and all on Squid.conf
squid::extra_config_section { 'ssl_bump settings':
  config_entries => {
    'ssl_bump'         => ['splice', 'all'],
  }
}

# intermediate class to configure squid certificates
class { 'intermediate': }


class intermediate {
  # Creating cert directory and changing the permissions
  file { '/etc/squid/certs':
    ensure => 'directory',
    owner  => 'squid',
    group  => 'squid',
    mode   => '0755',
  }
  # public and private cert are injecting to a single file
  exec { 'Redirecting the generated certs to /etc/squid/certs':
    command => 'cat /home/ec2-user/squid-ca-cert.pem /home/ec2-user/squid-ca-key.pem >> /etc/squid/certs/squid-ca-cert-key.pem',
    provider => shell,
    creates => '/etc/squid/certs/squid-ca-cert-key.pem',
  }
  # Updating the cert DB
  exec { 'Generate cert DB':
    command => '/usr/lib64/squid/ssl_crtd -c -s /var/lib/ssl_db',
    provider => shell,
    creates => '/var/lib/ssl_db',
  }
  # Changing cert DB directory permissions
  file { '/var/lib/ssl_db':
    ensure => 'directory',
    owner  => 'squid',
    group  => 'squid',
    recurse => true,
  }
}

# postinstall class to restarted squid service
class { 'postinstall': }

class postinstall {
  # Restart the squid service
  exec { 'restart squid':
    command => 'systemctl restart squid',
    provider => shell,
  }
}

